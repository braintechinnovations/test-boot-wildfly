package bootwildfly;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWildFlyController {


    @RequestMapping("hello")
    public String sayHello(){
        return ("Hello, SpringBoot on Wildfly");
    }
    
    @RequestMapping("test")
    public String sayHelloCiccio(){
        return ("Ciao sono Cicciopasticcio dal Guatemala");
    }

    @RequestMapping("studente")
    public String sayHelloStudente(){
        return ("Sono lo studente Giovanni Pace");
    }

    @RequestMapping("docente")
    public String sayHelloDocente(){
        return ("Sono il docente Giovanni Pace");
    }
    
    
}